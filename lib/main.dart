import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:vprokat/src/application.dart';
import 'package:vprokat/src/get_it_sl.dart' as service_locator;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await service_locator.init();
  await GetStorage.init();
  runApp(Application());
}

